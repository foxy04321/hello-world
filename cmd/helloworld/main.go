package main

import (
	"fmt"

	hw "gitlab.com/foxy04321/hello-world/internal/helloworld"
	Remotepkg "gitlab.com/foxy04321/hello-world/pkg/remotepkg"
)

func main() {
	err := run()
	if err != nil {
		fmt.Println(err)
	}
}

func run() error {
	hw.Print_hello_world()
	Remotepkg.Print_remote_pkg()
	return nil
}
